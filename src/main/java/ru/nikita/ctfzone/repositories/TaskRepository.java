package ru.nikita.ctfzone.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ru.nikita.ctfzone.domain.Task;
import ru.nikita.ctfzone.domain.TaskCategory;

import java.util.Optional;

@Repository
public interface TaskRepository extends PagingAndSortingRepository<Task, Long> {
    Optional<Task> findByNameEquals(String name);

    Page<Task> findAllByActiveEquals(Boolean active, Pageable pageable);

    Page<Task> findAllByActiveAndFullNameContainingIgnoreCase(Boolean active, String fullName, Pageable pageable);
    Page<Task> findAllByActiveAndCategory(Boolean active, TaskCategory category, Pageable pageable);

    Optional<Task> findByActiveTrueAndFlagEquals(String flag);

    Page<Task> findAllByFullNameContainingIgnoreCase(String fullName, Pageable pageable);
    Page<Task> findAllByCategory(TaskCategory category, Pageable pageable);
}
