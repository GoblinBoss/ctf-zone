package ru.nikita.ctfzone.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ru.nikita.ctfzone.domain.SolvedTask;
import ru.nikita.ctfzone.domain.Task;
import ru.nikita.ctfzone.domain.User;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface SolvedTaskRepository extends PagingAndSortingRepository<SolvedTask, Long> {
    Optional<SolvedTask> findByUserAndTask(User user, Task task);
    List<SolvedTask> findAllByUser(User user);
    List<SolvedTask> findAllByUserAndTaskIn(User user, Collection<Task> task);
}
