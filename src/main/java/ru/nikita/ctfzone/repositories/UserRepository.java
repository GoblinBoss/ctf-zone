package ru.nikita.ctfzone.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ru.nikita.ctfzone.domain.User;

import java.util.Optional;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long> {

    Optional<User> findByLoginEquals(String login);

    Page<User> findAllByLoginContainingIgnoreCase(String login, Pageable pageable);
    Page<User> findAllByLearnGroupContainingIgnoreCase(String learnGroup, Pageable pageable);
    Page<User> findAllByEmailContainingIgnoreCase(String login, Pageable pageable);


}
