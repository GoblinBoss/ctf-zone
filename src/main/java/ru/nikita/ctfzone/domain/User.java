package ru.nikita.ctfzone.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@EqualsAndHashCode(exclude = {"roles", "createdTasks", "solvedTasks"})
@ToString(exclude = {"roles", "createdTasks", "solvedTasks"})
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, unique = true)
    @NotNull
    private String login;
    @Column(nullable = false)
    @NotNull
    private String password;
    private String name;
    @Column(name = "last_name")
    private String lastName;
    private String patronymic;
    @Column(name = "learn_group")
    private String learnGroup;
    @Column(nullable = false)
    @NotNull
    @Email
    private String email;
    @Column(name = "registration_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date registrationDate;
    @Column(nullable = false)
    private Boolean admin;


    @Transient
    private String confirmPassword;

    @OneToMany(cascade = {CascadeType.DETACH, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REFRESH}, mappedBy = "creator")
    private Set<Task> createdTasks = new HashSet<>();

    @ManyToMany
    @JoinTable(name="users_roles", joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private Set<SolvedTask> solvedTasks = new HashSet<>();

}
