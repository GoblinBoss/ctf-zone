package ru.nikita.ctfzone.domain;

public enum TaskCategory {
    Forensic,
    Reverse,
    Pwn,
    Admin,
    Network,
    Crypto,
    Stegano,
    PPC,
    Web,
    Misc
}
