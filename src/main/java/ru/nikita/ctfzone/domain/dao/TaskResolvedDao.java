package ru.nikita.ctfzone.domain.dao;

import lombok.Builder;
import lombok.Data;
import ru.nikita.ctfzone.domain.Task;
import ru.nikita.ctfzone.domain.User;

import java.util.Date;

@Data
@Builder
public class TaskResolvedDao {
    private String name;
    private String fullName;
    private String link;
    private String category;
    private Date creationDate;
    private User creator;
    private Boolean resolved;
    private Date resolvedDate;

    public static TaskResolvedDao convertTaskToDao(Task task, boolean resolved, Date resolvedDate){
        TaskResolvedDaoBuilder builder = new TaskResolvedDaoBuilder();
        return builder.name(task.getName())
                .fullName(task.getFullName())
                .category(task.getCategory().name())
                .link(task.getLink())
                .creationDate(task.getCreationDate())
                .creator(task.getCreator())
                .resolved(resolved)
                .resolvedDate(resolvedDate)
                .build();
    }


}
