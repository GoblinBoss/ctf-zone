package ru.nikita.ctfzone.domain.dao;

import lombok.Data;
import lombok.NoArgsConstructor;
import ru.nikita.ctfzone.domain.Task;
import ru.nikita.ctfzone.domain.TaskCategory;

import java.util.Date;

@Data
//@Builder
@NoArgsConstructor
public class TaskDao {

    private Long id;
    private String name;
    private String flag;
    private String fullName;
    private String link;
    private Boolean active;
    private TaskCategory category;
    private Date creationDate;
    private String creator;

    public static TaskDao convertTaskToDao(Task task){
        TaskDao taskDao = new TaskDao();
        taskDao.setId(task.getId());
        taskDao.setName(task.getName());
        taskDao.setFlag(task.getFlag());
        taskDao.setFullName(task.getFullName());
        taskDao.setLink(task.getLink());
        taskDao.setActive(task.getActive());
        taskDao.setCategory(task.getCategory());
        taskDao.setCreationDate(task.getCreationDate());
        taskDao.setCreator(task.getCreator().getLogin());
        return taskDao;
    }
}
