package ru.nikita.ctfzone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

//@SpringBootApplication
//public class CtfZoneApplication {
//
//	public static void main(String[] args) {
//		SpringApplication.run(CtfZoneApplication.class, args);
//	}
//}
@SpringBootApplication
public class CtfZoneApplication extends SpringBootServletInitializer {
	public static void main(String[] args) {
		SpringApplication.run(CtfZoneApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(CtfZoneApplication.class);
	}
}