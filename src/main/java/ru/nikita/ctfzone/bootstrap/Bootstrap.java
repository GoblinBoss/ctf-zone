package ru.nikita.ctfzone.bootstrap;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import ru.nikita.ctfzone.domain.Role;
import ru.nikita.ctfzone.domain.User;
import ru.nikita.ctfzone.repositories.RoleRepository;
import ru.nikita.ctfzone.repositories.UserRepository;
import ru.nikita.ctfzone.services.TaskService;
import ru.nikita.ctfzone.services.UserService;


@Component
@Slf4j
public class Bootstrap implements ApplicationListener<ContextRefreshedEvent> {
    private RoleRepository roleRepository;
    private UserRepository userRepository;
    private UserService userService;
    private TaskService taskService;

    public Bootstrap(RoleRepository roleRepository, UserService userService, TaskService taskService) {
        this.roleRepository = roleRepository;
        this.roleRepository = roleRepository;
        this.userService = userService;
        this.taskService = taskService;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        try {
            if (userService.dbClear()){
                addEntities();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addEntities() throws Exception {
        Role adminRole = new Role();
        adminRole.setName(Role.ADMIN_ROLE);
        adminRole = roleRepository.save(adminRole);

        Role userRole = new Role();
        userRole.setName(Role.USER_ROLE);
        userRole = roleRepository.save(userRole);

        User adminUser = new User();
        adminUser.setLogin("adminUser");
        adminUser.setPassword("adminUser");
        adminUser.setAdmin(true);
        adminUser.setName("Admin");
        adminUser.setLastName("Adminus");
        adminUser.setPatronymic("Adminius");
        adminUser.setEmail("admin@mail.com");
        adminUser.getRoles().add(adminRole);
        userService.create(adminUser);

    }
}
