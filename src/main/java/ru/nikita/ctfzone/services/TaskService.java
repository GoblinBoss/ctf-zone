package ru.nikita.ctfzone.services;

import org.springframework.data.domain.Page;
import ru.nikita.ctfzone.domain.Task;
import ru.nikita.ctfzone.domain.TaskCategory;
import ru.nikita.ctfzone.domain.User;
import ru.nikita.ctfzone.domain.dao.TaskDao;
import ru.nikita.ctfzone.domain.dao.TaskResolvedDao;

import java.util.Collection;
import java.util.List;

public interface TaskService {
    Task createFromDao(TaskDao taskDao);
    Task updateFromDao(TaskDao taskDao, String name);
    void delete(Task task);
    Task findByName(String name);

    Page<Task> searchByNameSortedBy(Boolean active, String sortedBy, String search, int page, int limit);

    Page<Task> searchByCategorySortedBy(Boolean active, String sortedBy, TaskCategory category, int page, int limit);

    Task taskByFlag(String flag);

    boolean registrateFlag(User user, String flag);

    List<TaskResolvedDao> resolvedTasksByUser(Collection<Task> tasks, User user);
    List<TaskResolvedDao> resolvedTasksByUser(User user);
}
