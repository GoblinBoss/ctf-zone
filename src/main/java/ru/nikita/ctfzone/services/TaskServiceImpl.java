package ru.nikita.ctfzone.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.nikita.ctfzone.domain.SolvedTask;
import ru.nikita.ctfzone.domain.Task;
import ru.nikita.ctfzone.domain.TaskCategory;
import ru.nikita.ctfzone.domain.User;
import ru.nikita.ctfzone.domain.dao.TaskDao;
import ru.nikita.ctfzone.domain.dao.TaskResolvedDao;
import ru.nikita.ctfzone.repositories.SolvedTaskRepository;
import ru.nikita.ctfzone.repositories.TaskRepository;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TaskServiceImpl implements TaskService {

    private TaskRepository taskRepository;
    private SolvedTaskRepository solvedTaskRepository;
    private UserService userService;

    public TaskServiceImpl(TaskRepository taskRepository, SolvedTaskRepository solvedTaskRepository, UserService userService) {
        this.taskRepository = taskRepository;
        this.solvedTaskRepository = solvedTaskRepository;
        this.userService = userService;
    }


    @Override
    public Task createFromDao(TaskDao taskDao){
        Task task = new Task();
        task.setCreationDate(new Date());
        task.setName(taskDao.getName());
        task.setFlag(taskDao.getFlag());
        task.setFullName(taskDao.getFullName());
        task.setLink(taskDao.getLink());
        task.setActive(taskDao.getActive());
        task.setCategory(taskDao.getCategory());
        task.setCreator(userService.findByLogin(taskDao.getCreator()));
        return taskRepository.save(task);
    }

    @Override
    public Task updateFromDao(TaskDao taskDao, String name) {
        Task savedTask = findByName(name);
        if(savedTask == null){
            throw new RuntimeException("Task with this name does not exist");
        }
        savedTask.setName(taskDao.getName());
        savedTask.setFlag(taskDao.getFlag());
        savedTask.setFullName(taskDao.getFullName());
        savedTask.setLink(taskDao.getLink());
        savedTask.setActive(taskDao.getActive());
        savedTask.setCategory(taskDao.getCategory());
        savedTask.setCreator(userService.findByLogin(taskDao.getCreator()));
        return taskRepository.save(savedTask);
    }

    @Override
    public void delete(Task task) {
        taskRepository.deleteById(task.getId());
    }


    public Task findByName(String name){
        Optional<Task> task = taskRepository.findByNameEquals(name);
        return task.orElse(null);
    }

    @Override
    public Page<Task> searchByNameSortedBy(Boolean active, String sortedBy, String search, int page, int limit) {
        PageRequest pageRequest = PageRequest.of(page, limit, Sort.by(sortedBy));
        if(active != null){
            return taskRepository.findAllByActiveAndFullNameContainingIgnoreCase(active, search, pageRequest);
        }else{
            return taskRepository.findAllByFullNameContainingIgnoreCase(search, pageRequest);
        }

    }

    @Override
    public Page<Task> searchByCategorySortedBy(Boolean active, String sortedBy, TaskCategory category, int page, int limit) {
        PageRequest pageRequest = PageRequest.of(page, limit, Sort.by(sortedBy));
        if(active != null) {
            return taskRepository.findAllByActiveAndCategory(active, category, pageRequest);
        }else{
            return taskRepository.findAllByCategory(category, pageRequest);
        }
    }

    @Override
    public boolean registrateFlag(User user, String flag) {
        Task task = taskByFlag(flag);
        if(task == null) return false;
        Optional<SolvedTask> solvedTaskOptional = solvedTaskRepository.findByUserAndTask(user, task);
        if(solvedTaskOptional.isPresent()){
            SolvedTask solvedTask = solvedTaskOptional.get();
            solvedTask.setSolvedDate(new Date());
            solvedTaskRepository.save(solvedTask);
        }else{
            SolvedTask solvedTask = new SolvedTask();
            solvedTask.setUser(user);
            solvedTask.setTask(task);
            solvedTask.setSolvedDate(new Date());
            solvedTaskRepository.save(solvedTask);
        }
        return true;
    }

    @Override
    public List<TaskResolvedDao> resolvedTasksByUser(Collection<Task> tasks, User user) {
        List<SolvedTask> solvedTasks = solvedTaskRepository.findAllByUserAndTaskIn(user, tasks);
        return tasks.stream()
                .map(task -> {
                    Optional<SolvedTask> solvedTaskOptional = solvedTasks
                            .stream()
                            .filter(s -> s.getTask().getId().equals(task.getId()))
                            .findFirst();
                    return solvedTaskOptional.isPresent()?
                            TaskResolvedDao.convertTaskToDao(task, true, solvedTaskOptional.get().getSolvedDate()) :
                            TaskResolvedDao.convertTaskToDao(task, false, null);
                    })
                .collect(Collectors.toList());
    }

    @Override
    public List<TaskResolvedDao> resolvedTasksByUser(User user) {
        List<SolvedTask> solvedTasks = solvedTaskRepository.findAllByUser(user);
        return solvedTasks.stream()
                .map(task -> {
                    return TaskResolvedDao.convertTaskToDao(task.getTask(), true, task.getSolvedDate());
                })
                .collect(Collectors.toList());
    }


    @Override
    public Task taskByFlag(String flag) {
        return taskRepository.findByActiveTrueAndFlagEquals(flag).orElse(null);
    }
}
