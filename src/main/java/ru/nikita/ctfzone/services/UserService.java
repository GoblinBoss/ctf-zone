package ru.nikita.ctfzone.services;

import org.springframework.data.domain.Page;
import ru.nikita.ctfzone.domain.User;

public interface UserService {
    User create(User user) throws Exception;
    User update(User user, String login);
    void delete(User user);
    boolean dbClear();
    User findByLogin(String login);

    Page<User> searchBySortedBy(String searchBy, String sortedBy, String search, int page, int limit);

}
