package ru.nikita.ctfzone.validators;

import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.nikita.ctfzone.domain.Task;
import ru.nikita.ctfzone.domain.dao.TaskDao;
import ru.nikita.ctfzone.services.TaskService;
import ru.nikita.ctfzone.services.UserService;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Pattern;

    @Component
    public class TaskValidator implements Validator {
        private TaskService taskService;
        private UserService userService;

        public TaskValidator(TaskService taskService, UserService userService) {
            this.taskService = taskService;
            this.userService = userService;
        }

        @Override
        public boolean supports(Class<?> aClass) {
            return TaskDao.class.equals(aClass);
        }

        @Override
        public void validate(@Nullable Object o, Errors errors) {
            TaskDao taskDao = (TaskDao) o;
            Pattern patternName = Pattern.compile("^[a-zA-Z0-9_]*$");
            Pattern patternFlag = Pattern.compile("^[a-zA-Z0-9_{}|\\-]*$");

            if((taskDao.getName().length() < 6 || taskDao.getName().length() > 16)){
                errors.rejectValue("name", "error.name", "размер имени должен быть между 6 и 16 символами");
            }
            if(taskDao.getName().length() >= 6 && taskDao.getName().length() <= 16 && !patternName.matcher(taskDao.getName()).matches()){
                errors.rejectValue("name","error.name", "имя может содержать только символы \"a-Z A-Z 0-9 _\"");
            }
            Task existTask = taskService.findByName(taskDao.getName());
            if(existTask != null && !existTask.getId().equals(taskDao.getId())){
                errors.rejectValue("name","error.name", "задание с таким именем уже существует");
            }
            if((taskDao.getFullName().length() < 6 || taskDao.getFullName().length() > 64)){
                errors.rejectValue("fullName", "error.fullName", "размер полного имени должен быть между 6 и 64 символами");
            }
            if(taskDao.getFlag().length() != 36){
                errors.rejectValue("flag", "error.flag", "размер флага должен быть равным 36 символам");
            }
            if(!patternFlag.matcher(taskDao.getFlag()).matches()){
                errors.rejectValue("flag","error.flag", "флаг может содержать только символы \"a-Z A-Z 0-9 {}_|-\"");
            }
            Task existFlag = taskService.taskByFlag(taskDao.getFlag());
            if(existFlag != null && !existFlag.getId().equals(taskDao.getId())){
                errors.rejectValue("flag","error.flag", "даный флаг уже зарезервирован для другого задания");
            }
            try{
                URL url = new URL(taskDao.getLink());
            } catch (MalformedURLException e) {
                errors.rejectValue("link", "error.link", "не является ссылкой");
            }
            if(!taskDao.getCreator().isEmpty() && userService.findByLogin(taskDao.getCreator()) == null){
                errors.rejectValue("creator", "error.creator", "данного пользователя не существует");
            }


            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "error.name", "имя должно быть указано");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fullName", "error.fullName", "Полное имя должно быть указано");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "link", "error.link", "ссылка должна быть указано");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "creator", "error.creator", "создатель должен быть указан");
        }

        public void trimFields(TaskDao task) {
            task.setName(task.getName().trim());
            task.setFlag(task.getFlag().trim());
            task.setFullName(task.getFullName().trim());
            task.setLink(task.getLink().trim());
            task.setCreator(task.getCreator().trim());
        }
    }

