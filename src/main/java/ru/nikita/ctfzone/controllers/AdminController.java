package ru.nikita.ctfzone.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.nikita.ctfzone.domain.Task;
import ru.nikita.ctfzone.domain.TaskCategory;
import ru.nikita.ctfzone.domain.User;
import ru.nikita.ctfzone.domain.dao.TaskDao;
import ru.nikita.ctfzone.domain.dao.TaskResolvedDao;
import ru.nikita.ctfzone.services.TaskService;
import ru.nikita.ctfzone.services.UserService;
import ru.nikita.ctfzone.validators.TaskValidator;
import ru.nikita.ctfzone.validators.UserValidator;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@Controller
@Slf4j
public class AdminController {
    private TaskService taskService;
    private UserService userService;
    private UserValidator userValidator;
    private TaskValidator taskValidator;
    private final int LIMIT = 10;


    public AdminController(TaskService taskService, UserService userService, UserValidator userValidator, TaskValidator taskValidator) {
        this.taskService = taskService;
        this.userService = userService;
        this.userValidator = userValidator;
        this.taskValidator = taskValidator;
    }

    private User checkUser(Model model){
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userService.findByLogin(userDetails.getUsername());
        model.addAttribute("username", userDetails.getName());
        model.addAttribute("isAdmin", user.getAdmin());
        return user;
    }

    //                                                       \\
    // ============= Управление пользователями ============= \\
    //                                                       \\

    @RequestMapping(value = "/admin/users", method = RequestMethod.GET)
    public String getUsers(Model model){
        checkUser(model);
        return "redirect:/admin/users/search/1/login?search=&method=login";

    }



    @RequestMapping(value = "/admin/user/{login}", method = RequestMethod.GET)
    public String getUser(@PathVariable(value = "login") String login, Model model){
        checkUser(model);
        User user = userService.findByLogin(login);
        model.addAttribute("user", user);
        model.addAttribute("searchUser", login);
        return "admins/userDetail";
    }

    @RequestMapping(value = "/admin/user/update/{login}", method = RequestMethod.POST)
    public String postUser(@Valid @ModelAttribute("user") User user,@PathVariable(value = "login") String login, BindingResult result, Model model) {
        checkUser(model);
        userValidator.trimFields(user);
        userValidator.validate(user, result);
        if(result.hasErrors()){
            model.addAttribute("user", user);
            model.addAttribute("success", false);
            model.addAttribute("searchUser", login);
            return "admins/userDetail";
        }
        User savedUser = userService.update(user, login);
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
//        if(login.equals(userDetails.getUsername())) {
//            //@TODO исправит ошибку при изменении логина своего аккаунта
//        }
        model.addAttribute("user", savedUser);
        model.addAttribute("success", true);
        model.addAttribute("searchUser", user.getLogin());
        return "admins/userDetail";
    }

    @RequestMapping(value = "/admin/users/search")
    public String userSearchRedirect(@RequestParam(value = "search") String search, @RequestParam(value = "method") String method){
        return "redirect:/admin/users/search/1/registrationDate?search="+search+"&method="+method;
    }

    @RequestMapping(value = "/admin/users/search/{page}/{sortedBy}")
    public String search(@RequestParam(value = "search") String search,
                         @RequestParam(value = "method") String searchBy,
                         @PathVariable(value = "page") String page,
                         @PathVariable(value = "sortedBy") String sortedBy,
                         Model model){
        int pageNum = Integer.valueOf(page);
        if (pageNum < 1) return "redirect:/admin/users/1/"+sortedBy+"?search="+search+"&method="+searchBy;
        Page<User> users = userService.searchBySortedBy(searchBy, sortedBy, search, pageNum-1, LIMIT);
        if(pageNum-1 > users.getTotalPages()) return "redirect:/users/"+ users.getTotalPages() +"/"+sortedBy;
        checkUser(model);
        model.addAttribute("users", users);
        model.addAttribute("pageNumber", pageNum);
        model.addAttribute("sortedBy", sortedBy);
        model.addAttribute("method", searchBy);
        model.addAttribute("search", search);
        model.addAttribute("hasPrevious", pageNum != 1);
        model.addAttribute("hasNext", users.getTotalPages() != pageNum);
        return "admins/usersControl";
    }

    @RequestMapping(value = "/admin/user/delete/{login}", method = RequestMethod.POST)
    public String deleteUser(@PathVariable(value = "login") String login, Model model){
        checkUser(model);
        User user = userService.findByLogin(login);
        if (user != null){
            model.addAttribute("success", true);
            userService.delete(user);
        }else model.addAttribute("success", false);
        return "admins/deleteUser";
    }

    @RequestMapping(value = "/admin/user/solvedTask/{login}", method = RequestMethod.GET)
    public String search(@PathVariable(value = "login") String login, Model model){
        checkUser(model);
        List<TaskResolvedDao> solvedTasks = taskService.resolvedTasksByUser(userService.findByLogin(login));
        model.addAttribute("tasks", solvedTasks);
        model.addAttribute("user", login);
        return "admins/tasksSolved";
    }


    //                                                  \\
    // ============= Управление заданиями ============= \\
    //                                                  \\

    @RequestMapping(value = "admin/tasks", method = RequestMethod.GET)
    public String getTasks(Model model){
        return "redirect:tasks/search/1/fullName?search=&method=name";
    }

    @RequestMapping(value = "admin/tasks/search")
    public String taskSearchRedirect(@RequestParam(value = "search") String search, @RequestParam(value = "method") String method){
        return "redirect:/admin/tasks/search/1/fullName?search="+search+"&method="+method;
    }


    @RequestMapping(value = "admin/tasks/search/{page}/{sortedBy}", method = RequestMethod.GET)
    public String searchTasks(@RequestParam(value = "search") String search,
                              @RequestParam(value = "method") String searchBy,
                              @PathVariable(value = "sortedBy") String sortedBy,
                              @PathVariable(value = "page") String page, Model model){

        int pageNum = Integer.valueOf(page);
        if (pageNum < 1) return "redirect:admin/tasks/1/"+sortedBy;
        Page<Task> tasks;
        switch (searchBy){
            case "name":
                tasks = taskService.searchByNameSortedBy(null, sortedBy, search, pageNum-1, LIMIT);
                break;
            case "category":
                TaskCategory category;
                try{
                    category = TaskCategory.valueOf(search);
                    model.addAttribute("selectedCategory", category.name());
                }catch (IllegalArgumentException e){
                    category = TaskCategory.Admin;
                }
                tasks = taskService.searchByCategorySortedBy(null, sortedBy, category, pageNum-1, LIMIT);
                break;
            default:
                tasks = taskService.searchByNameSortedBy(null, sortedBy, search, pageNum-1, LIMIT);

        }
        if(pageNum-1 > tasks.getTotalPages()) return "redirect:admin/tasks/"+ tasks.getTotalPages() +"/"+sortedBy;
        User user = checkUser(model);
        model.addAttribute("categories", TaskCategory.values());
//        model.addAttribute("tasks", taskService.resolvedTasksByUser(tasks.getContent(), user));
        model.addAttribute("tasks", tasks);
        model.addAttribute("pageNumber", pageNum);
        model.addAttribute("sortedBy", sortedBy);
        model.addAttribute("method", searchBy);
        model.addAttribute("hasPrevious", pageNum != 1);
        model.addAttribute("hasNext", tasks.getTotalPages() != pageNum && tasks.getTotalPages()!= 0);
        model.addAttribute("countResults", tasks.getTotalElements());
        model.addAttribute("search", search);
        return "admins/tasksControl";
    }

    @RequestMapping(value = "/admin/task/{name}", method = RequestMethod.GET)
    public String getTask(@PathVariable(value = "name") String name, Model model){
        checkUser(model);
        Task task = taskService.findByName(name);
        model.addAttribute("task", TaskDao.convertTaskToDao(task));
        model.addAttribute("searchTask", name);
        return "admins/taskDetail";
    }

    @RequestMapping(value = "/admin/task/update/{name}", method = RequestMethod.POST)
    public String updateTask(@Valid @ModelAttribute("task") TaskDao taskDao,@PathVariable(value = "name") String name, BindingResult result, Model model) {
        checkUser(model);
        taskValidator.trimFields(taskDao);
        if(taskDao.getFlag().isEmpty()){
            taskDao.setFlag(UUID.randomUUID().toString());
        }
        taskValidator.validate(taskDao, result);
        if(result.hasErrors()){
            model.addAttribute("task", taskDao);
            model.addAttribute("success", false);
            model.addAttribute("searchTask", name);
            return "admins/taskDetail";
        }
        Task savedTask = taskService.updateFromDao(taskDao, name);
        model.addAttribute("task", TaskDao.convertTaskToDao(savedTask));
        model.addAttribute("success", true);
        model.addAttribute("searchTask", savedTask.getName());
        return "admins/taskDetail";
    }

    @RequestMapping(value = "/admin/task/delete/{name}", method = RequestMethod.POST)
    public String deleteTask(@PathVariable(value = "name") String name, Model model){
        checkUser(model);
        Task task = taskService.findByName(name);
        if (task != null){
            model.addAttribute("success", true);
            taskService.delete(task);
        }else model.addAttribute("success", false);
        return "admins/deleteTask";
    }

    @RequestMapping(value = "/admin/task/create", method = RequestMethod.GET)
    public String createTaskGet(Model model){
        checkUser(model);
        TaskDao task = new TaskDao();
        model.addAttribute("task", task);
        return "admins/taskCreate";
    }

    @RequestMapping(value = "/admin/task/create", method =RequestMethod.POST)
    public String createTaskPost(@Valid @ModelAttribute("task") TaskDao taskDao, BindingResult result, Model model){
        checkUser(model);
        taskValidator.trimFields(taskDao);
        if(taskDao.getFlag().isEmpty()){
            taskDao.setFlag(UUID.randomUUID().toString());
        }
        taskValidator.validate(taskDao, result);
        if(result.hasErrors()){
            model.addAttribute("task", taskDao);
            return "admins/taskCreate";
        }
        Task savedTask = taskService.createFromDao(taskDao);
        model.addAttribute("task", TaskDao.convertTaskToDao(savedTask));
        model.addAttribute("success", true);
        model.addAttribute("searchTask", savedTask.getName());
        return "admins/taskDetail";
    }

    @ExceptionHandler(NumberFormatException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public void handleException3(NumberFormatException ex, HttpServletResponse response) {

    }
}
