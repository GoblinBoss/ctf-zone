package ru.nikita.ctfzone.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.nikita.ctfzone.domain.Task;
import ru.nikita.ctfzone.domain.TaskCategory;
import ru.nikita.ctfzone.domain.User;
import ru.nikita.ctfzone.services.TaskService;
import ru.nikita.ctfzone.services.UserService;

import javax.servlet.http.HttpServletResponse;

@Controller
@Slf4j
public class UserController {
    private TaskService taskService;
    private UserService userService;
    private final int LIMIT = 10;

    public UserController(TaskService taskService, UserService userService) {
        this.taskService = taskService;
        this.userService = userService;
    }

    private User checkUser(Model model){
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userService.findByLogin(userDetails.getUsername());
        model.addAttribute("username", userDetails.getName());
        model.addAttribute("isAdmin", user.getAdmin());
        return user;
    }

    @RequestMapping(value = "/tasks", method = RequestMethod.GET)
    public String getTasks(Model model){
        return "redirect:/tasks/search/1/fullName?search=&method=name";
    }


    @RequestMapping(value = "/tasks/search")
    public String searchRedirect(@RequestParam(value = "search") String search, @RequestParam(value = "method") String method){
        return "redirect:/tasks/search/1/fullName?search="+search+"&method="+method;
    }

    @RequestMapping(value = "/tasks/search/{page}/{sortedBy}", method = RequestMethod.GET)
    public String searchTasks(@RequestParam(value = "search") String search,
                              @RequestParam(value = "method") String searchBy,
                            @PathVariable(value = "sortedBy") String sortedBy,
                            @PathVariable(value = "page") String page, Model model){
        int pageNum = Integer.valueOf(page);
        if (pageNum < 1) return "redirect:/tasks/1/"+sortedBy;
        Page<Task> tasks;
        switch (searchBy){
            case "name":
                tasks = taskService.searchByNameSortedBy(true, sortedBy, search, pageNum-1, LIMIT);
                break;
            case "category":
                TaskCategory category;
                try{
                    category = TaskCategory.valueOf(search);
                    model.addAttribute("selectedCategory", category.name());
                }catch (IllegalArgumentException e){
                    category = TaskCategory.Admin;
                }
                tasks = taskService.searchByCategorySortedBy(true, sortedBy, category, pageNum-1, LIMIT);
                break;
            default:
                tasks = taskService.searchByNameSortedBy(true, sortedBy, search, pageNum-1, LIMIT);

        }
        if(pageNum-1 > tasks.getTotalPages()) return "redirect:/tasks/"+ tasks.getTotalPages() +"/"+sortedBy;
        User user = checkUser(model);
        model.addAttribute("categories", TaskCategory.values());
        model.addAttribute("tasks", taskService.resolvedTasksByUser(tasks.getContent(), user));
        model.addAttribute("pageNumber", pageNum);
        model.addAttribute("sortedBy", sortedBy);
        model.addAttribute("method", searchBy);
        model.addAttribute("hasPrevious", pageNum != 1);
        model.addAttribute("hasNext", tasks.getTotalPages() != pageNum && tasks.getTotalPages()!= 0);
        model.addAttribute("countResults", tasks.getTotalElements());
        model.addAttribute("search", search);
        return "users/tasks";
    }

    @RequestMapping(value = "/flag", method = RequestMethod.GET)
    public String regFlagGet(Model model){
        checkUser(model);
        return "users/flag";
    }

    @RequestMapping(value = "/flag", method = RequestMethod.POST)
    public String regFlagPost(@RequestParam(value = "flag") String flag, Model model){
        flag = flag.trim();
        User user = checkUser(model);
        boolean reg = taskService.registrateFlag(user, flag);
        model.addAttribute("success", reg);
        return "users/flag";
    }

    @RequestMapping(value = "/user/{login}", method = RequestMethod.GET)
    public String getUserInfo(@PathVariable(value = "login") String login,  Model model){
        User user = checkUser(model);
        User userInfo = userService.findByLogin(login);
        model.addAttribute("user", userInfo);
        model.addAttribute("searchLogin", login);
        return "users/user";
    }

    @ExceptionHandler(NumberFormatException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public void handleException3(NumberFormatException ex, HttpServletResponse response) {

    }

}
